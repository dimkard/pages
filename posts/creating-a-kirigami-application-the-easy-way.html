<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="generator" content="Pelican" />
        <title>Creating a Kirigami application, the easy way</title>
        <link rel="stylesheet" href="../theme/css/main.css" />
        <link href="https://dimitris.cc/feed/all.xml" type="application/atom+xml" rel="alternate" title="dimitris.cc Atom Feed" />
        <meta name="description" content="Interested in getting started with Kirigami development in a few minutes? Since version 5.63 of the Kirigami framework, there is an easy way to do..." />
</head>

<body id="index" class="home">
        <header id="banner" class="body">
		<a class="site-avatar" href="../"><img src="/images/avatar-square.png"></a>
		<h1><a href="../">dimitris.cc</a></h1>
<h2>Notes on technology and freedom</h2>		<nav><ul>
                    <li><a href="../sitepages/about.html">About</a></li>
                </ul></nav>
        </header><!-- /#banner -->
<section id="content" class="body">
  <article>
    <header>
      <h1 class="entry-title">
        <a href="../posts/creating-a-kirigami-application-the-easy-way.html" rel="bookmark"
           title="Permalink to Creating a Kirigami application, the easy way">Creating a Kirigami application, the easy way</a></h1>
    </header>
    <p class="published">Νοε 11 2019</p>
    <div class="entry-content">
      <p>Interested in getting started with Kirigami development in a few minutes? Since version 5.63 of the <a href="https://kde.org/products/kirigami/">Kirigami framework</a>, there is an easy way to do so: an application template. The template facilitates the creation of a new application, using CMake as the build system, and linking to Kirigami dynamically as a plugin at runtime.</p>
<p>Let's see how it works. As a starting point, we will use a virtual environment running the latest <a href="https://neon.kde.org/download">KDE Neon User edition</a>. This is by no means a requirement, any Linux distribution with Kirigami 5.63 or later perfectly fits our needs. KDE Neon has been chosen just because it provides -by design- the latest Qt, KDE frameworks and applications. Moreover, a virtual machine will let us play fearlessly with the system directories.</p>
<p>At first, we install the packages required:</p>
<div class="highlight"><pre><span></span><code>sudo apt install qtbase5-dev build-essential git gcc g++ qtdeclarative5-dev qml-module-qtquick-controls libqt5svg5-dev qtmultimedia5-dev automake cmake qtquickcontrols2-5-dev libkf5config-dev libkf5service-dev libkf5notifications-dev libkf5kiocore5 libkf5kio-dev qml-module-qtwebengine gettext extra-cmake-modules libkf5wallet-dev qtbase5-private-dev qtwebengine5-dev libkf5wallet-dev qt5-default kirigami2-dev kdevelop kapptemplate
</code></pre></div>

<p>We are ready to convert the template to a working application. We will do it in two different ways:</p>
<ul>
<li>Using the <em>KAppTemplate</em> tool</li>
<li>With <em>KDevelop</em></li>
</ul>
<h1>The KAppTemplate way</h1>
<p>KAppTemplate, the Qt and KDE template generator, consumes templates and generates source code. After launching <em>KAppTemplate</em>, on the second page of the wizard, we select <em>Kirigami Application</em>. The Kirigami template can be found under <em>Qt &gt; Graphical</em> on the leftmost panel. We also provide a project name, e.g. hellokirigami.</p>
<p><img alt="Choose your project" class="blog-post-photo-centered-medium" src="../images/kapptemplate-project-template.png">
<em class="blog-post-photo-centered">Choose your project template</em></p>
<p>On the last page of the wizard, we set the application directory and, optionally, some details about our application. </p>
<p><img alt="Project properties" class="blog-post-photo-centered-medium" src="../images/kapptemplate-project-properties.png">
<em class="blog-post-photo-centered">Set the project properties</em></p>
<p>After clicking to <em>Generate</em>, the source code of our application is created; we are ready to start building it.</p>
<div class="highlight"><pre><span></span><code>cd /home/user/src/hellokirigami
mkdir build
cd build
cmake  ..
make -j4
sudo make install
</code></pre></div>

<p>Since the development environment is a virtual one, we are free to install our application in the system directories. Otherwise, in case of working on the host machine, custom install prefixes would be recommended.</p>
<p><img alt="Hello Kirigami" class="blog-post-photo-centered-medium" src="../images/hello-kirigami-desktop.png">
<em class="blog-post-photo-centered">Hello Kirigami</em></p>
<h1>Using KDevelop</h1>
<p>KDevelop is a full-featured IDE that integrates perfectly with application templates. On the <em>Project</em> menu, after clicking to the <em>New From Template</em> menu item, the application template wizard is shown. </p>
<p><img alt="New from template" class="blog-post-photo-centered" src="../images/kdevelop-new-from-template.png">
<em class="blog-post-photo-centered">Create a KDevelop project from template</em></p>
<p>We just select the <em>Qt</em> category on the leftmost panel and <em>Kirigami Application</em> on the right one, and set a project name, e.g. <em>hellokirigami2</em>. </p>
<p><img alt="Kirigami template" class="blog-post-photo-centered-medium" src="../images/kdevelop-kirigami-template.png">
<em class="blog-post-photo-centered">Kirigami template on KDevelop</em></p>
<p>On the last screen of the template wizard, we may also select a version control system, if needed. Not this time.</p>
<p>Back to KDevelop, we may set a custom installation prefix. Since we are not going to to install anything, we just select <em>Debug</em> as <em>Build type</em> and don't bother with installation prefixes.</p>
<p><img alt="KDevelop build dir" class="blog-post-photo-centered-medium" src="../images/kdevelop-build-dir.png">
<em class="blog-post-photo-centered">Set the build directory</em></p>
<p>The <em>Projects</em> pane enables us to examine the source code file hierarchy, while on the right we can see the code of each file. To test our application, we <em>Build</em> and, finally, <em>Execute</em> our application. Both actions can be found on the main toolbar.</p>
<p><img alt="KDevelop build exec" class="blog-post-photo-centered" src="../images/kdevelop-build-exec.png">
<em class="blog-post-photo-centered">Build and execute</em></p>
<p>Having clicked to <em>Execute</em>, on the <em>Launch Configurations</em> dialog, we <em>Add</em> a new configuration, selecting the <em>scr/hellokirigami2</em> project target. </p>
<p><img alt="Configure launch" class="blog-post-photo-centered" src="../images/kdevelop-set-target.png">
<em class="blog-post-photo-centered">Set application target on KDevelop</em></p>
<p>To check how the application will run on <a href="https://www.plasma-mobile.org/">Plasma Mobile</a>, we have to configure again a little bit our environment. In particular, on the <em>Run</em> menu, we click to the <em>Configure Launches</em> item. The configuration dialog will be displayed.</p>
<p><img alt="Configure environment" class="blog-post-photo-centered" src="../images/kdevelop-configure-env.png">
<em class="blog-post-photo-centered">Launch configurations on KDevelop</em></p>
<p>Then, we add the following to <em>Environment</em>:</p>
<ul>
<li>QT_QUICK_CONTROLS_MOBILE=true</li>
<li>QT_QUICK_CONTROLS_STYLE=Plasma</li>
</ul>
<p><img alt="Set environment variables" class="blog-post-photo-centered" src="../images/kdevelop-configure-env-vars.png">
<em class="blog-post-photo-centered">Set mobile environment variables</em></p>
<p>We are prompted with the mobile version of our application.</p>
<p><img alt="Hello Kirigami" class="blog-post-photo-centered-medium" src="../images/hello-kirigami-mobile.png">
<em class="blog-post-photo-centered">Hello Kirigami</em></p>
<p>That's all. It's time to add your own code bits. Happy hacking!</p>
    </div><!-- /.entry-content -->
  </article>
</section>
        <section id="extras" class="body">
        </section><!-- /#extras -->
        <section id="antisocial" class="body">
                <div class="social">
                            <a href="https://dimitris.cc/feed/all.xml" type="application/atom+xml" rel="alternate"><i class="social-icon rss"></i></a>
			    <!-- <a href="https://dimitris.cc/feed/all.xml" type="application/atom+xml" rel="alternate">feeds</a> -->

			<a href="https://floss.social/@dimitrisk"  rel="me"><i class="social-icon mastodon"></i></a>
			<!-- <a href="https://floss.social/@dimitrisk">floss.social</a> -->
			<a href="https://libretooth.gr/@dimitrisk"  rel="me"><i class="social-icon mastodon"></i></a>
			<!-- <a href="https://libretooth.gr/@dimitrisk">libretooth</a> -->
			<a href="https://codeberg.org/dimkard" ><i class="social-icon codeberg"></i></a>
			<!-- <a href="https://codeberg.org/dimkard">codeberg</a> -->
			<a href="https://invent.kde.org/dkardarakos" ><i class="social-icon kde"></i></a>
			<!-- <a href="https://invent.kde.org/dkardarakos">kde</a> -->
			<a href="https://gitlab.com/users/dimkard" ><i class="social-icon gitlab"></i></a>
			<!-- <a href="https://gitlab.com/users/dimkard">gitlab</a> -->
                </div><!-- /.social -->
        </section><!-- /#antisocial -->

        <footer id="contentinfo" class="body">
                <address id="about" class="vcard body">
			Powered by <a href="https://getpelican.com/">Pelican</a>. Theme based on <a rel="nofollow" href="https://github.com/getpelican/pelican/tree/main/pelican/themes/notmyidea">notmyidea</a>. Icons from <a rel "nofollow" href="https://github.com/simple-icons/simple-icons">simple-icons</a>.
                </address><!-- /#about -->
        </footer><!-- /#contentinfo -->

</body>
</html>